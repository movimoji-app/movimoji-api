// const { NotFound } = require('http-errors');
const RoomDao = require('../dao/room.dao');

/**
 * Users routes handling
 */
module.exports = {
  async findAll(request, reply) {
    return RoomDao.findAll()
  },
  async create(request, reply) {
    return RoomDao.create(request.body)
  },
};
