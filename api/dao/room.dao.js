const { db } = require('../../lib/db/db_client');
const { ROOMS } = require('./collections');

module.exports = {
  findAll: async () => {
    const rooms = db()
      .collection(ROOMS)
      .find()
      .toArray()
    return rooms
  },
  create: async ({ name }) => {
    const { insertedId: _id } = db()
      .collection(ROOMS)
      .insertOne({ name })
    return { _id, name }
  }
};
