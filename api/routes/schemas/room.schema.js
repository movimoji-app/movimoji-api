'use strict'

module.exports = fastify => fastify
  .addSchema({
    $id: 'room',
    type: 'object',
    properties: {
      _id: fastify.getSchema('objectid'),
      name: { type: 'string' }
    }
  })