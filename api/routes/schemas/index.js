const objectid = require('./objectid.schema')
const room = require('./room.schema')

module.exports = (fastify, opts) => {
  const schemas = [ objectid, room ]
  return schemas.reduce(
    (instance, schema) => schema(instance),
    fastify
  )
}