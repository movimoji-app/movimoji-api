'use strict'

const { findAll, create } = require('../controllers/room.controller')

module.exports = fastify => fastify
  .get('/rooms', {
    schema: {
      response: {
        200: {
          type: 'array',
          items: { $ref: 'room' }
        }
      }
    }
  }, findAll)
  
  .post('/rooms', {
    schema: {
      body: {
        type: 'object',
        required: ['title', 'description'],
        properties: {
          name: { type: 'string' },
        }
      },
      response: {
        200: { $ref: 'room' },
      }
    }
  }, create)
