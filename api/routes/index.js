const rooms = require('./rooms.routes')

module.exports = (fastify, opts) => {
  const routes = [ rooms ]
  return routes.reduce(
    (instance, route) => route(instance, opts),
    fastify.get('/', async function (request, reply) {
      return { version: '1.0.0' }
    })
  )
}