const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const config = require('../../config/config');

let db = null;

module.exports = {
  db: () => db,
  ObjectId: id => typeof id === 'string' ? ObjectId(id) : id,
  connect(url = config.db.url, dbName = config.db.name) {
    console.info('Connection to database...');
    return MongoClient.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then((mongo) => {
      db = mongo.db(dbName);
      console.info('Connected to database!');
      return Promise.resolve();
    }).catch((err) => {
      console.error('An error occured while connecting to mongo:', err);
      return Promise.reject(err);
    });
  },
};
