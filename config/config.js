module.exports = {
  host: process.env.HOST || 'locahost',
  port: process.env.PORT || 3000,
  db: {
    url: process.env.DATABASE_URL || 'mongodb://localhost:27017',
    name: process.env.DATABASE_NAME || 'movimoji',
  },
};
