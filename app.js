'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')
const dbClient = require('./lib/db/db_client')
const routes = require('./api/routes')
const schemas = require('./api/routes/schemas')

module.exports = async function (fastify, opts) {
  await dbClient.connect()

  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })

  schemas(fastify, opts)
  .register(routes, { options: Object.assign({}, opts) })
  .listen(3000, (err, address) => {
    if (err) throw err
    console.info(`Server is now listening on ${address}`)
  })
}
